import React from 'react';
import { Link } from 'react-router-dom';

const Main = () => {
	return (
		/* CONTEÚDO PRINCIPAL */
		<div className="container-fluid">
			<section className="principal bg-color-cinza">
				<div className="container-fluid">
					<div className="row">
					</div>
					<div className="col">
						<div className="row">
							<div className="col-md-3 col-sm-6">
								<div className="box align-middle">
									<img src="/img/icons/listar.svg" width="65" height="65" alt="Listar ações" />
									<p className="acoes text-center align-middle"><Link to="/projetos"><span className="font-weight-bold">Listar</span> ações</Link></p>
								</div>
							</div>
							<div className="col-md-3 col-sm-6">
								<div className="box">
									<img src="img/icons/criar.svg" width="65" height="65" alt="Criar ações" />
									<p className="acoes text-center align-middle"><Link to="/criar/novo"><span className="font-weight-bold">Criar</span> ações</Link></p>
								</div>
							</div>
							<div className="col-md-3 col-sm-6">
								<div className="box">
									<img src="img/icons/editar.svg" width="65" height="65" alt="Editar ações" />
									<p className="acoes text-center align-middle"><span className="font-weight-bold">Editar</span> ações</p>
								</div>
							</div>
							<div className="col-md-3 col-sm-6">
								<div className="box">
									<img src="img/icons/remover.svg" width="65" height="65" alt="Editar ações" />
									<p className="acoes text-center align-middle"><span className="font-weight-bold">Remover</span> ações</p>
								</div>
							</div>
						</div>
						<div className="row">
							<div className="col">
								<p><span className="badge badge-warning">Atenção<i className="fas fa-exclamation"></i></span> As <strong>Ações de Extensão</strong> só estarão disponíveis para os discentes após aprovação do CENEX.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	);
};

export default Main;
