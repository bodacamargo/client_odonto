import React, { Component } from 'react';
import {Criar} from '../services/Api'
import  { Redirect } from 'react-router-dom'

export default class CreateActions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			projectName: '',
			coord: '',
			subCoord: '',
			siex: '',
			startAt: '',
			startSubs: '',
			endSubs: '',
			description: '',
			schoolHist: false,
			studentEffic: false,
			outro: '',
			location: '',
			date: '',
			vagas: '',
			pbext: '',
			afirmativas: '',
			editar: false,
		};
	}

	componentWillMount() {
		const {id} = this.props.match.params;
		if(id !== 'novo'){
			this.setState(JSON.parse(id), 	this.setState({editar:true}))
		}

	}

	componentDidMount(){
		//console.log(this.state)
	}

	render() {
		const {
			projectName,
			coord,
			subCoord,
			siex,
			startAt,
			startSubs,
			endSubs,
			description,
			schoolHist,
			studentEffic,
			outro,
			location,
			date,
			vagas,
			pbext,
			afirmativas,
		} = this.state;

		return (
			/* CONTEÚDO PRINCIPAL */
			<div className="container">
				<section className="conteudo">
					<h3 className="text-center">Criar nova ação de extensão</h3>
					<div className="row">
						<div className="col">
							<form className="container">
								<div className="form-row">
									<div className="form-group col-md-12">
										<label htmlFor="formGroupExampleInput">Nome do Projeto/Programa</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Insira o nome do Projeto" onChange={(e) => { this.setState({ projectName: e.target.value }) }} value={projectName} />
									</div>
								</div>

								<div className="form-row">
									<div className="form-group col-md-6">
										<label htmlFor="formGroupExampleInput">Coordenador(a)</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Insira o nome do Coordenador(a)" onChange={(e) => { this.setState({ coord: e.target.value }) }} value={coord} />
									</div>
									<div className="form-group col-md-6">
										<label htmlFor="formGroupExampleInput">Subcoordenador(a)</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Insira o nome do Subcoordenador(a)" onChange={(e) => { this.setState({ subCoord: e.target.value }) }} value={subCoord} />
									</div>
								</div>

								<div className="form-row">
									<div className="form-group col-md-6">
										<label htmlFor="formGroupExampleInput">Número de registro do SIEX</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Insira o número de registro do SIEX" onChange={(e) => { this.setState({ siex: e.target.value }) }} value={siex} />
									</div>

									<div className="form-group col-md-6">
										<label htmlFor="formGroupExampleInput">Data Início do Projeto</label>
										<input type="date" className="form-control" id="formGroupExampleInput" placeholder="Insira a data de Início do projeto" onChange={(e) => { this.setState({ startAt: e.target.value }) }} value={startAt} />
									</div>
								</div>

								<div className="form-row">
									<div className="form-group col-md-6">
										<label htmlFor="formGroupExampleInput">Início Inscrição</label>
										<input type="date" className="form-control" id="formGroupExampleInput" placeholder="Insira a data de Início do projeto" onChange={(e) => { this.setState({ startSubs: e.target.value }) }} value={startSubs} />
									</div>

									<div className="form-group col-md-6">
										<label htmlFor="formGroupExampleInput">Termino Inscrição</label>
										<input type="date" className="form-control" id="formGroupExampleInput" placeholder="Insira a data de Início do projeto" onChange={(e) => { this.setState({ endSubs: e.target.value }) }} value={endSubs} />
									</div>
								</div>

								<div className="form-group">
									<div className="form-group shadow-textarea">
										<label htmlFor="exampleFormControlTextarea3">Resumo do Projeto/Programa</label>
										<textarea className="form-control" id="exampleFormControlTextarea3" rows="7" placeholder="Insira um breve resumo do Projeto/Programa" onChange={(e) => { this.setState({ description: e.target.value }) }} value={description}></textarea>
									</div>
								</div>

								<p>Documentação:</p>

								<div className="form-check col-md-4">
									<input className="form-check-input" type="checkbox" value="historico" id="defaultCheck1" checked={schoolHist}
										onChange={e => {
											this.setState({ schoolHist: !schoolHist });
										}} />
									<label className="form-check-label" htmlFor="defaultCheck1">Histórico escolar</label>
								</div>

								<div className="form-check col-md-4">
									<input className="form-check-input" type="checkbox" value="RSG" id="defaultCheck1" checked={studentEffic}
										onChange={e => {
											this.setState({ studentEffic: !studentEffic });
										}} />
									<label className="form-check-label" htmlFor="defaultCheck1">Rendimento Semestral Global</label>
								</div>

								<div className="form-row">
									<div className="form-group col-md-12">
										<label htmlFor="formGroupExampleInput">Outros</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Descrava os outros documentos necessários separados por virgula" onChange={(e) => { this.setState({ outro: e.target.value }) }} value={outro} />
									</div>
								</div>

								<div className="form-row">
									<div className="form-group col-md-12">
										<label htmlFor="formGroupExampleInput">Local de Funcionamento</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Descrava onde será realizado o projeto/programa" onChange={(e) => { this.setState({ location: e.target.value }) }} value={location} />
									</div>
								</div>

								<div className="form-row">
									<div className="form-group col-md-12">
										<label htmlFor="formGroupExampleInput">Dia e horário de funcionamento</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Descrava os dias da semana do projeto/programa" onChange={(e) => { this.setState({ date: e.target.value }) }} value={date} />
									</div>
								</div>
								<div className="form-row">
									<div className="form-group col-md-12">
										<label htmlFor="formGroupExampleInput">Vagas</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Descreva o número de vagas" onChange={(e) => { this.setState({ vagas: e.target.value }) }} value={vagas} />
									</div>
								</div>

								<p>Tipo de Bolsa:</p>

								<div className="form-check form-check-inline">
									<input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" checked={pbext}
										onChange={e => {
											this.setState({ pbext: !pbext });
										}} />
									<label className="form-check-label" htmlFor="inlineCheckbox1">PBEXT (R$ 400,00)</label>
								</div>
								<div className="form-check form-check-inline">
									<input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" checked={afirmativas}
										onChange={e => {
											this.setState({ afirmativas: !afirmativas });
										}} />
									<label className="form-check-label" htmlFor="inlineCheckbox2">AÇÕES AFIRMATIVAS (R$ 500,00)</label>
								</div>
								<button className="btn-primary container"
								onClick={
									 	(e) => {
										
											const response = Criar(this.state)
											e.preventDefault();
											
											window.location.href = '/projetos'
											console.log(response)
										
										}
								}> Cadastrar Projeto </button>
							</form>
							
						</div>
						
					</div>
					
				</section>
				

			</div>

		);
	}
}
