import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Listar as apiListar, Delete } from '../services/Api'

export default class Listar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      projects: []
    };

  }
  async loadLastProjects() {
    this.setState({ projects: await apiListar() })
  }

  componentDidMount() {
    this.loadLastProjects()

  }

  render() {

    const { projects } = this.state;

    return (


      <section className="conteudo container">
        <h3 className="text-center">Listar ações de extensão</h3>

        {!projects.length > 0
          ? ''
          : projects.map(project => (

            <div className="container-fluid" key={project.id}>
              <div className="row list-box">
                <div className="col-md-7">
                  <div className="row">
                    <p className="font-weight-light text-primary">{project.type}</p>
                  </div>
                  <div className="row">
                    <p className="font-weight-bold">{project.projectName}</p>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="row">
                    <p className="font-weight-light text-primary">Status</p>
                  </div>
                  <div className="row">
                    <p className="font-weight-bold">{project.status}</p>
                  </div>
                </div>
                <div className="col-md-2">
                  <div className="row">
                    <div className="align-middle">
                      <ul className="nav nav-pills">
                        <li className="nav-item dropdown">
                          <Link className="mt-1 nav-link dropdown-toggle active" data-toggle="dropdown" to="#" role="button" aria-haspopup="true" aria-expanded="false">Opções</Link>
                          <div className="dropdown-menu">
                            <Link className="dropdown-item" to="#">Visualizar</Link>
                            <Link className="dropdown-item" to="#">Publicar</Link>
                            <Link className="dropdown-item" to={`/criar/${JSON.stringify(project)}`}>Editar</Link>
                            <div className="dropdown-divider"></div>
                            <Link className="dropdown-item" to="#" onClick={ async () =>  { console.log(await Delete(project.id)); this.loadLastProjects()}}>Excluir</Link>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          ))}
      </section>)
  }
}