import React from 'react';
import {Link} from 'react-router-dom'

const Footer = () => {
  return (
    <footer className="footer">
        <div className="container-fluid">
            <h3 className="titulo-noticias">Últimas notícias do CENEX</h3>
            <div className="row footer-pad">
                <div className="col-md-4">
                    <div className="noticias">                      
                        <h4>Título da notícia aqui</h4>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </p>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="noticias">
                        <h4>Título da notícia aqui</h4>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </p>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="noticias">
                        <h4>Título da notícia aqui</h4>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </p>
                    </div>
                </div>
            </div>           
            <div className="row footer-direita footer-pad">
                <div className="col-md-6">
                    <p className="copy">2019 © Copyright -
                        <Link to="https://www.odonto.ufmg.br/cenex/" target="blank">CENEX Faculdade de Odontologia UFMG</Link>
                    </p>
                </div>
                <div className="col-md-6">
                    <p className="copy justify-content-end">Desenvolvido por:
                        <Link to="https://www.natiwo.com.br/" target="blank">NATIWO Agência Digital</Link>
                    </p>
                </div>          
            </div>
        </div>
    </footer>
  );
};

export default Footer;
