import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Header from './components/Header';
import Home from './pages/Home';
import Footer from './components/Footer';
import Criar from './pages/Criar';
import Listar from './pages/Listar'
import Login from './pages/Login'
//import express from 'express'

class App extends Component {

	/* componentDidMount() {
		 this.callApi()
			 .then(res => this.setState({ response: res.express }))
			 .catch(err => console.log(err));
	 }
	 callApi = async () => {
		 const response = await fetch('/api/hello');
		 const body = await response.json();
		 if (response.status !== 200) throw Error(body.message);
		 return body;
	 }; */

	render() {
		return (
			<BrowserRouter>
				<Header />

				<section className="principal bg-color-cinza">
					<div className="container">
						<div className="row">
							{/*CONTEUDO*/}
							
							<Route exact path="/" component={Home} />
							<Route path="/login" component={Login} />
							<Route path="/projetos" component={Listar} />
							<Route path="/criar/:id" component={Criar}/>

							{/*========*/}
						</div>
					</div>
				</section>
				<Footer />
			</BrowserRouter>
		);
	}
}

export default App;
